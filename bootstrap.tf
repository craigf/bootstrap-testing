variable "google_project" {}
variable "google_region" {}

provider "google" {
  project = "${var.google_project}"
  region  = "${var.google_region}"
}

module "craigf-bootstrap-test" {
  node_count          = 2
  bootstrap_version   = 9
  data_disk_count     = 9
  bootstrap_data_disk = "false"

  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[craigf-test]\""
  deletion_protection   = false
  data_disk_size        = 1
  data_disk_type        = "pd-standard"
  log_disk_size         = 1
  dns_zone_name         = "gitlab.com"
  egress_ports          = [80, 443]
  environment           = "craigf-test"
  ip_cidr_range         = "10.224.200.0/24"
  machine_type          = "n1-standard-4"
  name                  = "craigf-test"
  os_disk_type          = "pd-ssd"
  project               = "${var.google_project}"
  public_ports          = [22]
  service_account_email = "terraform@gitlab-staging-1.iam.gserviceaccount.com"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=craigf/configurable-disk-count"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = "gstg"
  region                = "${var.google_region}"
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-gstg-chef-bootstrap"
    bootstrap_key     = "gitlab-gstg-bootstrap-validation"
    bootstrap_keyring = "gitlab-gstg-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.22.5"
  }
}
